/*
    Ubuntu.ir Pastebin Socket Server
    Copyright (C) 2020 Moein Alinaghian <nixoeen@nixoeen.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#define PORT 1337
#define PATH "/home/paste/tmp"

#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/stat.h> 
#include <fcntl.h>

void paste(int sock)
{
	int i,n;
	int random_fd = open("/dev/random", O_RDONLY);
	unsigned char random_data[4];
	size_t random_data_len = 0;
	char name[256];

	FILE *fd;
	char buffer[1024];

	char command[1024];
	char url[256];

	while (random_data_len < sizeof random_data)
	{
		ssize_t result = read(random_fd, random_data, (sizeof random_data) - random_data_len);
		if (result < 0)
			return;
		random_data_len += result;
	}
	close(random_fd);
	strcpy(name, PATH);
	strcat(name, "/");
	for (i=0; i < 4; i++)
		sprintf(name, "%s%02x", name, (unsigned int) random_data[i]);

	fd = fopen(name, "wb");
	while (n = read(sock,buffer,1024))
	{
		if (n < 0)
		{
			perror("Failed: Reading from the socket");
			exit(1);
		}
		fwrite(buffer, sizeof(char), n, fd);
	}
	fclose(fd);

	strcpy(command, "/bin/cat ");
	strcat(command, name);
	strcat(command, " | /usr/bin/curl -s -F 'text=<-' https://paste.ubuntu.ir");
	fd = popen(command, "r");
	if (fd == NULL)
	{
		perror("Failed: Running the command");
		exit(1);
	}
	while (fgets(url, sizeof(url)-1, fd) != NULL)
	{
		i = write(sock, url, strlen(url));
		if (n < 0)
		{
			perror("Failed: Writing to socket");
			exit(1);
		}
	}
	fclose(fd);
	unlink(name);
}

int main( int argc, char *argv[] )
{
	int sockfd, newsockfd, portno, clilen;
	char buffer[256];
	struct sockaddr_in serv_addr, cli_addr;
	int  n, pid;
   
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0)
	{
		perror("Failed: Opening Socket");
		exit(1);
	}
 
	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(PORT);
 
	if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
	{
		perror("Failed: Binding");
		exit(1);
	}
   
	listen(sockfd,5);
	clilen = sizeof(cli_addr);
 
	while (1)
	{
		newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
		if (newsockfd < 0)
		{
			perror("Failed: Accepting Connection");
			exit(1);
		}
      
		pid = fork();
		if (pid < 0)
		{
			perror("Failed: Fork");
			exit(1);
		}
      
		if (pid == 0)
		{
			close(sockfd);
			paste(newsockfd);
			exit(0);
		}
		else
			close(newsockfd);
	}
}
